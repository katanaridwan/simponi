library dashboard;

import 'package:bkpp/app/constans/app_constants.dart';
import 'package:bkpp/app/shared_components/card_task.dart';
import 'package:bkpp/app/shared_components/header_text.dart';
import 'package:bkpp/app/shared_components/list_task_assigned.dart';
import 'package:bkpp/app/shared_components/list_task_date.dart';
import 'package:bkpp/app/shared_components/responsive_builder.dart';
import 'package:bkpp/app/shared_components/search_field.dart';
import 'package:bkpp/app/shared_components/selection_button.dart';
import 'package:bkpp/app/shared_components/simple_selection_button.dart';
import 'package:bkpp/app/shared_components/simple_user_profile.dart';
import 'package:bkpp/app/shared_components/task_progress.dart';
import 'package:bkpp/app/shared_components/user_profile.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:bkpp/app/utils/helpers/app_helpers.dart';
import 'package:intl/intl.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:bkpp/app/shared_components/user_profile.dart';

// binding
part '../../bindings/dashboard_binding.dart';

// controller
part '../../controllers/dashboard_controller.dart';

// model

// component
part '../components/bottom_navbar.dart';
part '../components/header_weekly_task.dart';
part '../components/main_menu.dart';
part '../components/task_menu.dart';
part '../components/member.dart';
part '../components/task_in_progress.dart';
part '../components/weekly_task.dart';
part '../components/task_group.dart';

class DashboardScreen extends GetView<DashboardController> {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: controller.scafoldKey,
      // drawer: ResponsiveBuilder.isDesktop(context)
      //     ? null
      //     : Drawer(
      //         child: SafeArea(
      //           child: SingleChildScrollView(child: _buildSidebar(context)),
      //         ),
      //       ),
      appBar: buildAppBar(context),
      bottomNavigationBar: (ResponsiveBuilder.isDesktop(context) || kIsWeb)
          ? null
          : const _BottomNavbar(),
      body: SafeArea(
        child: ResponsiveBuilder(
          mobileBuilder: (context, constraints) {
            return SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _buildTaskContent(
                    onPressedMenu: () => controller.openDrawer(),
                  ),
                  // _buildCalendarContent(),
                ],
              ),
            );
          },
          tabletBuilder: (context, constraints) {
            return Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                  flex: constraints.maxWidth > 800 ? 8 : 7,
                  child: SingleChildScrollView(
                    controller: ScrollController(),
                    child: _buildTaskContent(
                      onPressedMenu: () => controller.openDrawer(),
                    ),
                  ),
                ),
                // SizedBox(
                //   height: MediaQuery.of(context).size.height,
                //   child: const VerticalDivider(),
                // ),
                // Flexible(
                //   flex: 4,
                //   child: SingleChildScrollView(
                //     controller: ScrollController(),
                //     child: _buildCalendarContent(),
                //   ),
                // ),
              ],
            );
          },
          desktopBuilder: (context, constraints) {
            return Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Flexible(
                //   flex: constraints.maxWidth > 1350 ? 3 : 4,
                //   child: SingleChildScrollView(
                //     controller: ScrollController(),
                //     child: _buildSidebar(context),
                //   ),
                // ),
                Flexible(
                  flex: constraints.maxWidth > 1350 ? 10 : 9,
                  child: SingleChildScrollView(
                    controller: ScrollController(),
                    child: _buildTaskContent(),
                  ),
                ),
                // SizedBox(
                //   height: MediaQuery.of(context).size.height,
                //   child: const VerticalDivider(),
                // ),
                // Flexible(
                //   flex: 4,
                //   child: SingleChildScrollView(
                //     controller: ScrollController(),
                //     child: _buildCalendarContent(),
                //   ),
                // ),
              ],
            );
          },
        ),
      ),
    );
  }

  // Widget _buildSidebar(BuildContext context) {
  //   return Column(
  //     crossAxisAlignment: CrossAxisAlignment.start,
  //     children: [
  //       Padding(
  //         padding: const EdgeInsets.symmetric(horizontal: 10),
  //         child: UserProfile(
  //           data: controller.dataProfil,
  //           onPressed: controller.onPressedProfil,
  //         ),
  //       ),
  //       const SizedBox(height: 15),
  //       Padding(
  //         padding: const EdgeInsets.symmetric(horizontal: 10),
  //         child: _MainMenu(onSelected: controller.onSelectedMainMenu),
  //       ),
  //       const Divider(
  //         indent: 20,
  //         thickness: 1,
  //         endIndent: 20,
  //         height: 60,
  //       ),
  //       _Member(member: controller.member),
  //       const SizedBox(height: kSpacing),
  //       _TaskMenu(
  //         onSelected: controller.onSelectedTaskMenu,
  //       ),
  //       const SizedBox(height: kSpacing),
  //       Padding(
  //         padding: const EdgeInsets.all(kSpacing),
  //         child: Text(
  //           "Djagat Apps 2021",
  //           style: Theme.of(context).textTheme.caption,
  //         ),
  //       ),
  //     ],
  //   );
  // }

  Widget _buildTaskContent({Function()? onPressedMenu}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: kSpacing),
      child: Column(
        children: [
          const SizedBox(height: kSpacing),
          Row(
            children: [
              if (onPressedMenu != null)
                Padding(
                  padding: const EdgeInsets.only(right: kSpacing / 2),
                  child: IconButton(
                    onPressed: onPressedMenu,
                    icon: const Icon(Icons.menu),
                  ),
                ),
              // Expanded(
              //   child: SearchField(
              //     onSearch: controller.searchTask,
              //     hintText: "Search",
              //   ),
              // ),
            ],
          ),
          const SizedBox(height: kSpacing),
          Column(children: [
            // Text("BIDANG TTT"),
            _TaskInProgress(
<<<<<<< HEAD
                title: "BIDANG FORJAB",color:Color(0xffFFD8D8), data: controller.taskInProgress),
          ]),
          const SizedBox(height: kSpacing * 1),
          _TaskInProgress(
              title: "BIDANG DIKLAT",color:Color(0xffC6D8FC),  data: controller.taskInProgress),
              const SizedBox(height: kSpacing * 1),
          _TaskInProgress(
              title: "BIDANG BINKES",color:Color(0xffC6D8FC),  data: controller.taskInProgress),
              const SizedBox(height: kSpacing * 1),
              _TaskInProgress(
              title: "BIDANG KINERJA DAN MUTASI",color:Color(0xffC6D8FC),  data: controller.taskInProgress),
=======
                title: "BIDANG ABCD",color:Color(0xffFFD8D8), data: controller.taskInProgress),
          ]),
          const SizedBox(height: kSpacing * 1),
          _TaskInProgress(
              title: "BIDANG ABCD",color:Color(0xffC6D8FC),  data: controller.taskInProgress),
              const SizedBox(height: kSpacing * 1),
          _TaskInProgress(
              title: "BIDANG ABCD",color:Color(0xffC6D8FC),  data: controller.taskInProgress),
              const SizedBox(height: kSpacing * 1),
              _TaskInProgress(
              title: "BIDANG ABCD",color:Color(0xffC6D8FC),  data: controller.taskInProgress),
>>>>>>> 438d04675736dfa54c6e6d8d02b8b7bd0bff65be
              const SizedBox(height: kSpacing * 1),

          // const _HeaderWeeklyTask(),
          // const SizedBox(height: kSpacing),
          // _WeeklyTask(
          //   data: controller.weeklyTask,
          //   onPressed: controller.onPressedTask,
          //   onPressedAssign: controller.onPressedAssignTask,
          //   onPressedMember: controller.onPressedMemberTask,
          // )

        ],
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      centerTitle: true,
      titleSpacing: 0.0,
      title: Container(
        // color: Colors.white,
        // decoration: new BoxDecoration(
        //   borderRadius: new BorderRadius.circular(16.0),
        //   color: Colors.white,
        // ),

        child: Row(
          children: [
            const SizedBox(width: kSpacing),
            Image.asset('lg_pati_big.png',
                width: 40, height: 40, fit: BoxFit.fill),
            Expanded(
              child: HeaderText(
                " DASHBOARD",
              ),
            ),
            const SizedBox(width: kSpacing / 2),
            // SizedBox(
            //   width: 300,
            //   child: TaskProgress(data: controller.dataTask),
            // ),

            Icon(
              Icons.search,
              color: Theme.of(context).primaryColor,
              size: 30.0,
            ),
            Icon(
              Icons.mode_night,
              color: Theme.of(context).primaryColor,
              size: 30.0,
            ),
            Icon(
              Icons.notifications,
              color: Theme.of(context).primaryColor,
              size: 30.0,
            ),
            const SizedBox(width: kSpacing),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              
              children: [
              Text(
                'Muhamad Ridwan Huseyni',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
              ),
              Text(
                'Super Admin',
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
            ]),
            Image.asset('ic_person.png',
                width: 40, height: 40, fit: BoxFit.fill),
          ],
        ),
      ),
      backgroundColor: Theme.of(context).primaryColorLight,
    );
  }
  // Widget _buildCalendarContent() {
  //   return Padding(
  //     padding: const EdgeInsets.symmetric(horizontal: kSpacing),
  //     child: Column(
  //       children: [
  //         const SizedBox(height: kSpacing),
  //         Row(
  //           children: [
  //             const Expanded(child: HeaderText("Calendar")),
  //             IconButton(
  //               onPressed: controller.onPressedCalendar,
  //               icon: const Icon(EvaIcons.calendarOutline),
  //               tooltip: "calendar",
  //             )
  //           ],
  //         ),
  //         const SizedBox(height: kSpacing),
  //         ...controller.taskGroup
  //             .map(
  //               (e) => _TaskGroup(
  //                 title: DateFormat('d MMMM').format(e[0].date),
  //                 data: e,
  //                 onPressed: controller.onPressedTaskGroup,
  //               ),
  //             )
  //             .toList()
  //       ],
  //     ),
  //   );
  // }
}
