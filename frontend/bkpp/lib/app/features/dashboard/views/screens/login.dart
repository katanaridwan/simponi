<<<<<<< HEAD
import 'dart:convert';
import 'package:flutter/material.dart';
// import 'package:elayang_flutter/service/elayang/Service_login.dart';
// import 'package:elayang_flutter/screens/elayang/Dashboard.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:firebase_core/firebase_core.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  var nip;
  var password;
  var device;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  // _showMsg(msg) {
  //   final snackBar = SnackBar(
  //     content: Text(msg),
  //     action: SnackBarAction(
  //       label: 'Close',
  //       onPressed: () {
  //         // Some code to undo the change!
  //       },
  //     ),
  //   );
  //   _scaffoldKey.currentState.showSnackBar(snackBar);
  // }

  void initState() {
    super.initState();
    // _setFirebase();
    // getDeviceId();
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return WillPopScope(
      onWillPop: () {
        return new Future(() => false);
      },
      child: Scaffold(
        key: _scaffoldKey,
        body: Table(columnWidths: {
          0: FlexColumnWidth(10),
          1: FlexColumnWidth(5),
        }, children: [
          TableRow(children: [
            Image.asset('login.png', fit: BoxFit.fill),
             Padding(
                          padding: const EdgeInsets.all(25.0),
                          child: Form(
                            key: _formKey,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                // Container(
                                //   width: 100.0,
                                //   height: 100.0,
                                //   decoration: new BoxDecoration(
                                //       //  color: const Color(0xff7c94b6),
                                //       shape: BoxShape.circle,
                                //       image: new DecorationImage(
                                //           fit: BoxFit.fill,
                                //           image: new AssetImage(
                                //               'assets/lg_pati_big.png'))),
                                // ),
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 2.0,top:220),
                                  child: Text("SIMPONI",
                                      style: TextStyle(
                                          color: Colors.green,
                                          fontSize: 30,
                                          fontWeight: FontWeight.bold)),
                                ),
                                Text(
                                    "Sistem Informasi Kepegawaian Online",
                                    style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 15,
                                    )),
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 25.0),
                                  child: Text("",
                                      style: TextStyle(
                                          color: Colors.grey,
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold)),
                                ),
                                TextFormField(
                                  style: TextStyle(color: Color(0xFF000000)),
                                  cursorColor: Color(0xFF9b9b9b),
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      Icons.account_circle_sharp,
                                      color: Colors.grey,
                                    ),
                                    hintText: "Username",
                                    hintStyle: TextStyle(
                                        color: Color(0xFF9b9b9b),
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal),
                                  ),
                                ),
                                SizedBox(
                                              height: 20.0,
                                              width: 20.0,),
                                TextFormField(
                                  style: TextStyle(color: Color(0xFF000000)),
                                  cursorColor: Color(0xFF9b9b9b),
                                  keyboardType: TextInputType.text,
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      Icons.vpn_key,
                                      color: Colors.grey,
                                    ),
                                    hintText: "Password",
                                    hintStyle: TextStyle(
                                        color: Color(0xFF9b9b9b),
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal),
                                  ),
                                ),
                                SizedBox(
                                              height: 20.0,
                                              width: 20.0,),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * .90,
                                  padding: const EdgeInsets.all(10.0),
                                  child: FlatButton(
                                    // minWidth: 400,
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                          top: 15,
                                          bottom: 15,
                                          left: 10,
                                          right: 10),
                                      child: _isLoading == false
                                          ? Text(
                                              'SIGN IN',
                                              textDirection: TextDirection.ltr,
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 17.0,
                                                decoration: TextDecoration.none,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            )
                                          : SizedBox(
                                              height: 20.0,
                                              width: 20.0,
                                              child: CircularProgressIndicator(
                                                valueColor:
                                                    AlwaysStoppedAnimation<
                                                        Color>(Colors.white),
                                              ),
                                            ),
                                    ),
                                    color: Colors.green,
                                    disabledColor: Colors.grey,
                                    shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(30.0)),
                                    onPressed: () {
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
          ]),
        ]),
      ),
    );
  }
}
=======
import 'dart:convert';
import 'package:flutter/material.dart';
// import 'package:elayang_flutter/service/elayang/Service_login.dart';
// import 'package:elayang_flutter/screens/elayang/Dashboard.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:firebase_core/firebase_core.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  var nip;
  var password;
  var device;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  // _showMsg(msg) {
  //   final snackBar = SnackBar(
  //     content: Text(msg),
  //     action: SnackBarAction(
  //       label: 'Close',
  //       onPressed: () {
  //         // Some code to undo the change!
  //       },
  //     ),
  //   );
  //   _scaffoldKey.currentState.showSnackBar(snackBar);
  // }

  void initState() {
    super.initState();
    // _setFirebase();
    // getDeviceId();
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return WillPopScope(
      onWillPop: () {
        return new Future(() => false);
      },
      child: Scaffold(
        key: _scaffoldKey,
        body: Table(columnWidths: {
          0: FlexColumnWidth(10),
          1: FlexColumnWidth(5),
        }, children: [
          TableRow(children: [
            Image.asset('login.png', fit: BoxFit.fill),
             Padding(
                          padding: const EdgeInsets.all(25.0),
                          child: Form(
                            key: _formKey,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                // Container(
                                //   width: 100.0,
                                //   height: 100.0,
                                //   decoration: new BoxDecoration(
                                //       //  color: const Color(0xff7c94b6),
                                //       shape: BoxShape.circle,
                                //       image: new DecorationImage(
                                //           fit: BoxFit.fill,
                                //           image: new AssetImage(
                                //               'assets/lg_pati_big.png'))),
                                // ),
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 2.0,top:220),
                                  child: Text("SIMPONI",
                                      style: TextStyle(
                                          // color: Colors.white,
                                          fontSize: 30,
                                          fontWeight: FontWeight.bold)),
                                ),
                                Text(
                                    "Sistem Informasi Kepegawaian Online",
                                    style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 15,
                                    )),
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 25.0),
                                  child: Text("",
                                      style: TextStyle(
                                          color: Colors.grey,
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold)),
                                ),
                                TextFormField(
                                  style: TextStyle(color: Color(0xFF000000)),
                                  cursorColor: Color(0xFF9b9b9b),
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      Icons.account_circle_sharp,
                                      color: Colors.grey,
                                    ),
                                    hintText: "Username",
                                    hintStyle: TextStyle(
                                        color: Color(0xFF9b9b9b),
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal),
                                  ),
                                ),
                                SizedBox(
                                              height: 20.0,
                                              width: 20.0,),
                                TextFormField(
                                  style: TextStyle(color: Color(0xFF000000)),
                                  cursorColor: Color(0xFF9b9b9b),
                                  keyboardType: TextInputType.text,
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      Icons.vpn_key,
                                      color: Colors.grey,
                                    ),
                                    hintText: "Password",
                                    hintStyle: TextStyle(
                                        color: Color(0xFF9b9b9b),
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal),
                                  ),
                                ),
                                SizedBox(
                                              height: 20.0,
                                              width: 20.0,),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * .90,
                                  padding: const EdgeInsets.all(10.0),
                                  child: FlatButton(
                                    // minWidth: 400,
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                          top: 15,
                                          bottom: 15,
                                          left: 10,
                                          right: 10),
                                      child: _isLoading == false
                                          ? Text(
                                              'SIGN IN',
                                              textDirection: TextDirection.ltr,
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 17.0,
                                                decoration: TextDecoration.none,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            )
                                          : SizedBox(
                                              height: 20.0,
                                              width: 20.0,
                                              child: CircularProgressIndicator(
                                                valueColor:
                                                    AlwaysStoppedAnimation<
                                                        Color>(Colors.white),
                                              ),
                                            ),
                                    ),
                                    color: Colors.green,
                                    disabledColor: Colors.grey,
                                    shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(30.0)),
                                    onPressed: () {
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
          ]),
        ]),
      ),
    );
  }
}
>>>>>>> 438d04675736dfa54c6e6d8d02b8b7bd0bff65be
