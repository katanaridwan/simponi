part of dashboard;

class _TaskInProgress extends StatelessWidget {
  const _TaskInProgress({
    required this.title,
    required this.color,
    required this.data,
    Key? key,
  }) : super(key: key);
  final String title;
  final Color color;
  final List<CardTaskData> data;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(kBorderRadius * 0),
      child: 
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                title,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
      SizedBox(
        height: 220,
        // width: 350,

        child:
        
              Container(
          color: color,
          padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
          child:
              
              ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            physics: const BouncingScrollPhysics(),
            itemCount: data.length,
            itemBuilder: (context, index) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: kSpacing / 2),
              child: CardTask(
                data: data[index],
                primary: _getSequenceColor(index),
                onPrimary: Colors.white,
              ),
            ),
          ),
          
        ),

          
      ),],
          ),
    );
  }

  Color _getSequenceColor(int index) {
    int val = index % 4;
    if (val == 3) {
      return Colors.red;
    } else if (val == 2) {
      return Colors.orange;
    } else if (val == 1) {
      return Colors.blue;
    } else {
      return Colors.green;
    }
  }
}
