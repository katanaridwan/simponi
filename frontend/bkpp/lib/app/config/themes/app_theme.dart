import 'package:bkpp/app/constans/app_constants.dart';
import 'package:flutter/material.dart';

/// all custom application theme
class AppTheme {
  /// default application theme
  static ThemeData get light => ThemeData(
        fontFamily: Font.nunito,
        canvasColor: Color(0xffF1F1F1),
        primarySwatch: Colors.grey,
        primaryColorLight:Colors.white
      );
      static ThemeData get dark => ThemeData(
        fontFamily: Font.nunito,
        canvasColor: Colors.black,
        primarySwatch: Colors.indigo,
      );

  // you can add other custom theme in this class like  light theme, dark theme ,etc.

  // example :
  // static ThemeData get light => ThemeData();
  // static ThemeData get dark => ThemeData();
}
