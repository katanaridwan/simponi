<div class="content-wrapper">
    <div class="card">
        <div class="card-body">
            <div class="btn-group pull-right">
                <a href="<?= base_url('sistem/user/add') ?>" class="btn btn-success">
                    <i class="fa fa-plus"></i> Tambah Baru
                </a>
            </div>
            <div class="col-sm-16">
                <!-- <h3 class="card-title">Daftar Pengguna</h3> -->
                <div class="row">
                    <div class="card"> </div>
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-paginate" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nip</th>
                                            <th>UserName</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($user as $data){
                                            ?>
                                        <tr>
                                            <td>
                                            <?= $data->UserId?>
                                            </td>
                                            <td><?= $data->UserNip?></td>
                                            <td>
                                            <?= $data->UserName?>
                                            </td>
                                            <td>
                                                <!-- <div class="dropdown">
                                                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                                                        <i data-feather="more-vertical"></i>
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="#">
                                                            <i data-feather="edit-2" class="me-50"></i>
                                                            <span>Edit</span>
                                                        </a>
                                                        <a class="dropdown-item" href="#">
                                                            <i data-feather="trash" class="me-50"></i>
                                                            <span>Delete</span>
                                                        </a>
                                                    </div>
                                                </div> -->
                                                <!-- <a href="<?= base_url('sistem/group/update/'.$data->UserId);?>" class="btn btn-primary">
                                                            <i data-feather="edit-2" class="me-50"></i>
                                                        </a>
                                                        <a href="<?= base_url('sistem/group_do/delete/'.$data->UserId);?>" class="btn btn-danger">
                                                            <i data-feather="trash" class="me-50"></i>
                                                        </a> -->


                                                        <div class="btn-group" role="group" aria-label="Basic example">
                                                        <a href="<?= base_url('sistem/group/update/'.$data->UserId);?>" >
                                                <button type="button" class="btn btn-primary waves-effect waves-float waves-light"><i data-feather="edit-2" class="me-50"></i></button>
                                        </a>
                                        <a href="<?= base_url('sistem/group_do/delete/'.$data->UserId);?>" >
                                                <button type="button" class="btn btn-danger waves-effect waves-float waves-light"><i data-feather="trash" class="me-50"></i></button>
                                                <!-- <button type="button" class="btn btn-primary waves-effect waves-float waves-light">Right</button> -->
                                        </a>
                                            </div>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?= base_url("template/backend/js/jquery.js") ?>"></script>

    <script src="<?= base_url('assets/');?>app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js"></script>
    <script src="<?= base_url('assets/');?>app-assets/vendors/js/tables/datatable/dataTables.bootstrap5.min.js"></script>
    <script src="<?= base_url('assets/');?>app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
    <script src="<?= base_url('assets/');?>app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js"></script>
    <script src="<?= base_url('assets/');?>app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js"></script>


<!-- Bootstrap CSS -->
<!-- Bootstrap DataTables CSS -->
<!-- Jquery -->
<script type="text/javascript" language="javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<!-- Jquery DataTables -->
<script type="text/javascript" language="javascript" src="http:////cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js"></script>
<!-- Bootstrap dataTables Javascript -->
<script type="text/javascript" language="javascript" src="http://cdn.datatables.net/plug-ins/9dcbecd42ad/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <script type="text/javascript">

    $(document).ready(function() {
	$('.table-paginate').dataTable();
 } );
        // $(function() {
        //     $("#data-table").tabel({
        //         source: '<?= base_url("sistem/user/search") ?>',
        //         filterParams: $("div#data-table_wrapper").find("select").serializeArray(),
        //         order: [
        //             [2, 'asc']
        //         ],
        //         columns: [{
        //                 bVisible: false,
        //                 data: 'UserId'
        //             },
        //             {
        //                 title: 'Aksi',
        //                 data: 'aksi'
        //             },
        //             {
        //                 title: 'NIP',
        //                 data: 'UserNip'
        //             },
        //             {
        //                 title: 'Nama Lengkap',
        //                 data: 'UserRealName'
        //             },
        //             {
        //                 title: 'Username',
        //                 data: 'UserName'
        //             },
        //             {
        //                 title: 'e-mail',
        //                 data: 'UserEmail'
        //             },
        //             {
        //                 title: 'Berlaku',
        //                 data: 'UserExpired'
        //             },
        //             {
        //                 title: 'Aktif',
        //                 data: 'UserActive'
        //             }
        //         ]
        //     });
        // });

        // function hapus(id) {
        //     bootbox.confirm({
        //         message: "Apakah anda yakin akan menghapus data Pengguna ini?",
        //         buttons: {
        //             cancel: {
        //                 label: '<i class="fa fa-times"></i> Batal',
        //                 className: "btn-danger btn-sm"
        //             },
        //             confirm: {
        //                 label: '<i class="fa fa-check"></i> Yakin',
        //                 className: "btn-primary btn-sm"
        //             }
        //         },
        //         callback: function(result) {
        //             if (result) {
        //                 $.post(
        //                     '<?= base_url("sistem/user_do/delete") ?>', {
        //                         id: id
        //                     },
        //                     function(data) {
        //                         if (data.success) {
        //                             $.gritter.add({
        //                                 title: 'Informasi',
        //                                 text: 'Pengguna berhasil dihapus.',
        //                                 class_name: 'gritter-info gritter-center'
        //                             });
        //                             $("#data-table").tabel({
        //                                 reload: true
        //                             });
        //                         } else {
        //                             bootbox.alert(data.msg);
        //                         }
        //                     }, "json"
        //                 );
        //             }
        //         }
        //     });
        // }

        // function reset(id) {
        //     bootbox.confirm({
        //         message: "Apakah anda yakin akan mereset data Pengguna ini?",
        //         buttons: {
        //             cancel: {
        //                 label: '<i class="fa fa-times"></i> Batal',
        //                 className: "btn-danger btn-sm"
        //             },
        //             confirm: {
        //                 label: '<i class="fa fa-check"></i> Yakin',
        //                 className: "btn-primary btn-sm"
        //             }
        //         },
        //         callback: function(result) {
        //             if (result) {
        //                 $.post(
        //                     '<?= base_url("sistem/user/delete") ?>', {
        //                         id: id
        //                     },
        //                     function(data) {
        //                         if (data.success) {
        //                             $.gritter.add({
        //                                 title: 'Informasi',
        //                                 text: 'Pengguna berhasil direset.',
        //                                 class_name: 'gritter-info gritter-center'
        //                             });
        //                             $("#data-table").tabel({
        //                                 reload: true
        //                             });
        //                         } else {
        //                             bootbox.alert(data.msg);
        //                         }
        //                     }, "json"
        //                 );
        //             }
        //         }
        //     });
        // }
    </script>