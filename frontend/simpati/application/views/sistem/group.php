<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <div class="btn-group pull-right">
          <a href="<?= base_url('sistem/group/add') ?>" class="btn btn-success">
              <i class="fa fa-plus"></i> Tambah Baru
          </a>
      </div>
      <div class="col-sm-16">
      <!-- <h3 class="card-title">Daftar Group</h3> -->
        <div class="row">
         <div class="card"> </div> 
            <div class="table-responsive">   
            <!-- <table class="table table-bordered" id="data-table"></table>   -->
            <table class="table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($group as $data){?>
                                        <tr>
                                            <td>
                                            <?= $data->GroupId?>
                                            </td>
                                            <td><?= $data->GroupName?></td>
                                            <td>
                                            <?= $data->GroupDescription?>
                                            </td>
                                            <td>
                                                <!-- <div class="dropdown">
                                                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                                                        <i data-feather="more-vertical"></i>
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="#">
                                                            <i data-feather="edit-2" class="me-50"></i>
                                                            <span>Edit</span>
                                                        </a>
                                                        <a class="dropdown-item" href="#">
                                                            <i data-feather="trash" class="me-50"></i>
                                                            <span>Delete</span>
                                                        </a>
                                                    </div>
                                                </div> -->
                                                <!-- <a href="<?= base_url('sistem/group/update/'.$data->GroupId);?>" class="btn btn-primary">
                                                            <i data-feather="edit-2" class="me-50"></i>
                                                        </a>
                                                        <a href="<?= base_url('sistem/group_do/delete/'.$data->GroupId);?>" class="btn btn-danger">
                                                            <i data-feather="trash" class="me-50"></i>
                                                        </a> -->


                                                        <div class="btn-group" role="group" aria-label="Basic example">
                                                        <a href="<?= base_url('sistem/group/update/'.$data->GroupId);?>" >
                                                <button type="button" class="btn btn-primary waves-effect waves-float waves-light"><i data-feather="edit-2" class="me-50"></i></button>
                                        </a>
                                        <a href="<?= base_url('sistem/group_do/delete/'.$data->GroupId);?>" >
                                                <button type="button" class="btn btn-danger waves-effect waves-float waves-light"><i data-feather="trash" class="me-50"></i></button>
                                                <!-- <button type="button" class="btn btn-primary waves-effect waves-float waves-light">Right</button> -->
                                        </a>
                                            </div>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
          </div>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript" src="<?=base_url("template/backend/js/jquery.js")?>"></script>
<script type="text/javascript">
    $(function() {
        $("#data-table").tabel({
            source: '<?=base_url("sistem/group/search")?>',
            filterParams: $("div#data-table_wrapper").find("select").serializeArray(),
            order: [[ 2, 'asc' ]],
            columns: [		            	
                {bVisible: false,data :'GroupId'},                
                {title: 'Aksi' , data : 'aksi'},
                {title: 'Nama' , data : 'GroupName'},
                {title: 'Keterangan', data : 'GroupDescription'}
            ]
        });
    });
    
    function hapus(id) {
        bootbox.confirm({
            message: "Apakah anda yakin akan menghapus file lampiran ini?",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Batal',
                    className: "btn-danger btn-sm"
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Yakin',
                    className: "btn-primary btn-sm"        
                }
            },
            callback: function (result) {
                if(result) {
                    $.post(
                        '<?=base_url("sistem/group_do/delete")?>',
                        {id: id},
                        function(data) {
                            if(data.success) {
                                $.gritter.add({
                                    title: 'Informasi',
                                    text: 'Group berhasil dihapus.',
                                    class_name: 'gritter-info gritter-center'
                                });
                                $("#data-table").tabel({
                                    reload :true                                
                                });
                            } else {
                                bootbox.alert(data.msg);
                            }
                        }, "json"
                    );
                }
            }
        });                
    }
</script>    
