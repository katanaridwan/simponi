<form action="<?=base_url("sistem/user_do/$sub")?>" method="post">
    <div class="panel panel-bordered panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon md-assignment-account"></i> Form <?=$sub == 'add' ? 'Tambah' : 'Edit'?> Pengguna</h3>
            
        </div>
        <div class="panel-body">    
            <div class="form-group">
                <label class="form-control-label">NIP</label>
                <input type="text" class="form-control" name="UserNip" id="UserNip" placeholder="NIP" value="<?= $user['UserNip']; ?>" />
            </div>
            
            <div class="form-group">
                <label class="form-control-label">Nama Lengkap</label>
                <input type="text" class="form-control" name="UserRealName" id="UserRealName" placeholder="Nama Lengkap" value="<?= $user['UserRealName']; ?>" />
            </div>
            
            <div class="form-group">
                <label class="form-control-label">Username</label>
                <input type="text" class="form-control" name="UserName" id="UserName" placeholder="UserName" value="<?= $user['UserName']; ?>" />
            </div>
            
            <div class="form-group">
                <label class="form-control-label">Password</label>
                <input type="password" class="form-control" name="UserPassword" id="UserPassword" placeholder="Password"  />
            </div>
            
            <div class="form-group">
                <label class="form-control-label">Ulangi Password</label>
                <input type="password" class="form-control" name="UlangPassword" id="UlangPassword" placeholder="Ulangi Password"  />
            </div>

            <div class="form-group">
                <label for="int" class="form-control-label">Group</label>
                <select id="groups" class="form-control" name="groups[]" data-plugin="select2">
                    <option value="">-- Pilih Group --</option>
                    <?php if( count($groups) ): ?>
                    <?php foreach($groups as $item): ?>
                        <option value="<?= $item['GroupId'] ?>" <?=in_array($item['GroupId'], $usergroup) ? 'selected':''?>>
                            <?= $item['GroupName'] ?>
                        </option>
                    <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            
            <div class="form-group">
                <label for="int" class="form-control-label">Unit Kerja</label>
                <select id="UserUnitKerjaId" class="form-control" name="UserUnitKerjaId" data-plugin="select2">
                    <option value="">-- Pilih Unit Kerja --</option>
                    <?php if( count($unitkerja) ): ?>
                    <?php foreach($unitkerja as $item): ?>
                        <option value="<?= $item['UnitKerjaId'] ?>" <?=$item['UnitKerjaId'] == $user['UserUnitKerjaId'] ? 'selected':''?>><?= $item['UnitKerjaNama'] ?></option>
                    <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>

            <div class="form-group">
                <label class="form-control-label">Nomor HP</label>
                <input type="text" class="form-control" name="UserHp" id="UserHp" placeholder="Nomor Hp" value="<?= $user['UserHp']; ?>" />
            </div>
            
            <div class="form-group">
                <label class="form-control-label">e-mail</label>
                <input type="text" class="form-control" name="UserEmail" id="UserEmail" placeholder="e-mail" value="<?= $user['UserEmail']; ?>" />
            </div>

            <div class="form-group">
                <label for="varchar" class="form-control-label">Masa Berlaku </label>
                <input type="text" name="UserExpired" class="form-control" data-format="dd/mm/yyyy" data-plugin="datepicker" value="<?=$user['UserExpired']?>">
            </div>

            <div class="form-group">
                <label class="form-control-label">Status</label>
                <div class="checkbox">
                    <label>
                        <input id="UserActive" name="UserActive" type="checkbox" value="1"<?=$user['UserActive'] ? ' checked="checked"' : ''?>>
                        Aktif
                    </label>
                </div>
            </div>

            <div class="form-group">
                <label class="form-control-label">Keterangan</label>
                <input type="text" class="form-control" name="UserNote" id="UserNote" placeholder="Keterangan" value="<?= $user['UserNote']; ?>" />
            </div>
        
            <input type="hidden" name="UserId" value="<?=$user['UserId']?>"/>
            <div class="mt-50" style="float:right">
                <a href="<?=base_url('sistem/user')?>" onclick="javascript: return confirm('Yakin Anda akan membatalkan?')" class="btn btn-danger waves-effect waves-classic waves-effect waves-classic">Batal</a>
                <button type="submit" class="btn btn-success waves-effect waves-classic waves-effect waves-classic">Simpan</button>
            </div>
        </div>
    </div>
</form>