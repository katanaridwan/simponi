<div class="panel  panel-bordered panel-primary">
    <div class="panel-heading">
        <!-- <h3 class="panel-title"><i class="icon md-assignment-account"></i> Daftar User</h3> -->
        <div class="panel-actions ">
        <a class="panel-action icon md-minus" data-toggle="panel-collapse" aria-hidden="true"></a>
        </div>
    </div>
    <div class="panel-body">
        <div class="row mb-4">
            <div class="col-sm-12 col-md-4">
                <div class="btn-toolbar" aria-label="Toolbar with button groups" role="toolbar">
                    <div class="btn-group" role="group">
                        <a href="<?= base_url('sistem/user/add') ?>" class="btn btn-icon btn-primary waves-effect waves-classic">
                        <i class="icon md-plus" aria-hidden="true"></i> Tambah Baru
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-8">
                <form id="foms" class="form-inline pull-right mb-20" autocomplete="off" action="<?=$search?>">
                    <div class="form-group ml-10">
                        <div class="input-group">
                            <input type="text" class="form-control" name="q" value="<?=$this->input->get('q')?>" placeholder="Cari...">
                            <span class="input-group-append">
                                <button type="submit" class="btn btn-primary waves-effect waves-classic"><i data-feather="search" class="me-50"></i></button>
                            </span>
                        </div>
                    </div>
                    <input type="hidden" name="start" value="<?=$start?>" />
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-hover dataTable table-striped w-full">
                <thead>
                    <tr>
                        <th>Aksi</th>
                        <th>NIP</th>
                        <th>Nama Lengkap</th>
                        <th>Username</th>
                        <th>eMail</th>
                        <th>Berlaku</th>
                        <th>Aktif</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    if(count($rows)){
                        foreach($rows as $no => $item) {
                            ?>
                            <tr>
                                <td style='width:8%; text-align:center'>
                                <div class="btn-toolbar" aria-label="Toolbar with button groups" role="toolbar">
                                    <div class="btn-group" role="group">
                                        <a href="<?= base_url('sistem/user/update/'.$item['UserId']) ?>" class="btn btn-icon btn-success"><i data-feather="edit-2" class="me-50"></i></a>
                                        <a href="<?= base_url('sistem/user_do/delete/'.$item['UserId']) ?>" onclick="javascript: return confirm('Anda Yakin Hapus?')" class="btn btn-icon btn-danger"><i data-feather="trash" class="me-50"></i></a> 
                                        
                                    </div>
                                </div>    
                                </td>
                                <td><?= $item['UserNip']; ?></td>
                                <td><?= $item['UserRealName']; ?></td>
                                <td><?= $item['UserName']; ?></td>
                                <td><?= $item['UserEmail']; ?></td>
                                <td><?= $item['UserExpired']; ?></td>
                                <td><?= $item['UserActive']; ?></td>
                            </tr>
                            <?php
                        }
                    }
                ?>
            </table>    
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 mx-auto">
            <?= $pagination ?>
        </div>
    </div>
</div>

