<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class System_model extends Ci_Model {

   function getMenu($uid) {
      $sql = "
         SELECT MenuId,MenuParentId,MenuModule,MenuName,MenuIcon,MenuHasSubmenu
         FROM pati_menu
         INNER JOIN pati_menu_aksi ON MenuId=MenuAksiMenuId
         INNER JOIN pati_group_menu_aksi ON MenuAksiId=GroupMenuMenuAksiId
         WHERE GroupMenuGroupId IN (SELECT UserGroupGroupId FROM pati_user_group WHERE UserGroupUserId = ?) AND MenuIsShow=1
         GROUP BY MenuId
         ORDER BY MenuOrder";
      $query = $this->db->query($sql, array($uid));
      return $query->result_array();
   }

}

/*
 * End of file : system_model.php
 * File location : ./models/system_model.php
 */