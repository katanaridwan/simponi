<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_model extends CI_Model
{

    public $table = 'pati_user';
    public $id = 'nip';
	public $order = 'DESC';
	
	const LEVEL = [
		1 => 'Admin',
		2 => 'Pimpinan',
		3 => 'Tata Usaha',
		4 => 'Staf',
		5 => 'Sekretaris',
	];

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
	}
	
	public function list_penerima($aksi = '', $query = [])
	{
		$query = array_merge([
			'where_not_in' => [],
			'level' => 2,
		], $query);
        $this->load->library('Datatables');
		
		if(count($query['where_not_in'])){
			foreach($query['where_not_in'] as $key => $value)
				$this->datatables->where_not_in($key, $value);
		}
		$this->datatables->select('
			nip, 
			instansi as id_instansi, 
			nama_lengkap, 
			kode_lokasi,
			master_kantor.nmjabatan as nama_jabatan, 
			master_kantor.nmlokasi as nama_instansi
		');
        $this->datatables->from($this->table);
		$this->datatables->join('master_kantor', 'master_kantor.kdlokasi = '.$this->table.'.kode_lokasi');
		$this->datatables->where('level', $query['level']);
        $this->datatables->add_column('aksi', $aksi, 'nip');
        return $this->datatables->generate();
	}
	
	public function list_tembusan($aksi = '')
	{
		$this->load->library('Datatables');
		
		$this->datatables->select('kdlokasi as id_instansi, nmlokasi as nama_instansi, kdlokasi, nmlokasi');
        $this->datatables->from('master_kantor');
        $this->datatables->add_column('aksi', $aksi, 'kdlokasi');
        return $this->datatables->generate();
	}

	/**
	 * Get kepala Instansi
	 * @param Int|String $instansi
	 * @return Collection $collection
	 */
	public function kepala($instansi)
	{
		$this->db->where(['kode_lokasi' => $instansi, 'kode_jabatan' => '20000']);
		return $this->db->get($this->table)->row();
	}
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('nip', $q);
	$this->db->or_like('instansi', $q);
	$this->db->or_like('nama_lengkap', $q);
	$this->db->or_like('nohp', $q);
	$this->db->or_like('email', $q);
	$this->db->or_like('foto', $q);
	$this->db->or_like('kode_lokasi', $q);
	$this->db->or_like('kode_jabatan', $q);
	$this->db->or_like('nama_jabatan', $q);
	$this->db->or_like('kode_eselon', $q);
	$this->db->or_like('golongan', $q);
	$this->db->or_like('password', $q);
	$this->db->or_like('level', $q);
	$this->db->or_like('akses', $q);
	$this->db->or_like('kop_surat', $q);
	$this->db->or_like('ttd', $q);
	$this->db->or_like('ttd_stempel', $q);
	$this->db->or_like('ttd_p12', $q);
	$this->db->or_like('ttd_logo', $q);
	$this->db->or_like('status', $q);
	$this->db->or_like('keyword', $q);
	$this->db->or_like('keterangan', $q);
	$this->db->or_like('firebase_web', $q);
	$this->db->or_like('firebase_app', $q);
	$this->db->or_like('firebase_info', $q);
	$this->db->or_like('last_sync', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('nip', $q);
	$this->db->or_like('instansi', $q);
	$this->db->or_like('nama_lengkap', $q);
	$this->db->or_like('nohp', $q);
	$this->db->or_like('email', $q);
	$this->db->or_like('foto', $q);
	$this->db->or_like('kode_lokasi', $q);
	$this->db->or_like('kode_jabatan', $q);
	$this->db->or_like('nama_jabatan', $q);
	$this->db->or_like('kode_eselon', $q);
	$this->db->or_like('golongan', $q);
	$this->db->or_like('password', $q);
	$this->db->or_like('level', $q);
	$this->db->or_like('akses', $q);
	$this->db->or_like('kop_surat', $q);
	$this->db->or_like('ttd', $q);
	$this->db->or_like('ttd_stempel', $q);
	$this->db->or_like('ttd_p12', $q);
	$this->db->or_like('ttd_logo', $q);
	$this->db->or_like('status', $q);
	$this->db->or_like('keyword', $q);
	$this->db->or_like('keterangan', $q);
	$this->db->or_like('firebase_web', $q);
	$this->db->or_like('firebase_app', $q);
	$this->db->or_like('firebase_info', $q);
	$this->db->or_like('last_sync', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
   //  function insert($data)
   //  {
   //      $this->db->insert($this->table, $data);
   //  }

   //  // update data
   //  function update($id, $data)
   //  {
   //      $this->db->where($this->id, $id);
   //      $this->db->update($this->table, $data);
   //  }

   //  // delete data
   //  function delete($id)
   //  {
   //      $this->db->where($this->id, $id);
   //      $this->db->delete($this->table);
   //  }
	//  function countUser(){
	// 	 return $this->db->get($this->table)->num_rows();

	//  }

    function getUser($key, $start, $limit) {
		$this->db->select("*, DATE_FORMAT(UserExpired, '%d-%m-%Y') AS `Expired`", FALSE);
		$this->db->from('pati_user');
		// $this->db->join('yk_group', 'GroupId=UserGroupId', 'left');
		$this->db->like('UserNip', $key);
		$this->db->or_like('UserName', $key);
		$this->db->or_like('UserRealName', $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get();
//      print_r($this->db->last_query());
//      print_r($this->db->count_all_results());
		return $query->result_array();
  }

  function countUser($key) {
		$this->db->select("COUNT(UserId) AS total", FALSE);
		$this->db->from('pati_user');
		// $this->db->join('yk_group', 'GroupId=UserGroupId', 'left');
		$this->db->like('UserNip', $key);
		$this->db->or_like('UserName', $key);
		$this->db->or_like('UserRealName', $key);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result[0]['total'];
  }

  function getDataTables($status = '', $aksi = '') {
		$this->load->library('Datatables');

		$this->datatables->select("UserId, UserNip, UserRealName, UserName, UserPassword, UserActive, UserEmail, UserFoto, DATE_FORMAT(UserExpired, '%d-%m-%Y') AS UserExpired");
		$this->datatables->from("pati_user");
		$this->datatables->add_column('aksi', $aksi, 'UserId');
		return $this->datatables->generate();
  }

  function getById($id) {
		$this->db->select("UserId, UserNip, UserRealName, UserName, UserPassword, UserActive, UserEmail, UserHp, UserFoto, DATE_FORMAT(UserExpired, '%d-%m-%Y') AS UserExpired, UserNote");
		$this->db->from("pati_user");
		$this->db->where("UserId", $id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result[0];
  }

  // get data with limit and search
//   function get_limit_data($limit, $start = 0, $q = NULL) {
// 		$this->db->order_by('UserId', 'desc');
// 		$this->db->limit($limit, $start);
// 		return $this->db->get('pati_user')->result();
//   }

  function getAll() {
		$query = $this->db->get('pati_user');
		return $query->result_array();
  }
  
  function getUserGroupsById($id) {
		$query = $this->db->get_where('pati_user_group', array('UserGroupUserId' => $id));
		return $query->result_array();
  }

  function add($data, $groups) {
		$this->db->trans_start();
		$this->db->set($data);
		$this->db->insert('pati_user');
		$id = $this->db->insert_id();
		for($i=0;$i<sizeof($groups);$i++) {
			 $this->db->insert('pati_user_group', array('UserGroupUserId' => $id, 'UserGroupGroupId' => $groups[$i]));
		}
		$this->db->trans_complete();
		if ($this->db->trans_status()) {
			 return $id;
		} else {
			 return 0;
		}
  }

  function update($data, $groups, $id) {
		$this->db->trans_start();
		$this->db->set($data);
		$this->db->where('UserId', $id);
		$this->db->update('pati_user');
		$this->db->delete('pati_user_group', array('UserGroupUserId' => $id));
		for($i=0;$i<sizeof($groups);$i++) {
			 $this->db->insert('pati_user_group', array('UserGroupUserId' => $id, 'UserGroupGroupId' => $groups[$i]));
		}
		$this->db->trans_complete();
		return $this->db->trans_status();
  }

  function delete($id) {
		$this->db->trans_start();
		$this->db->delete('pati_user_group', array('UserGroupUserId' => $id));
		$this->db->delete('pati_user', array('UserId' => $id));
		$this->db->trans_complete();
		return $this->db->trans_status();
  }

  function getUniqueUserByUsername($username, $id) {
		$sql = "
		 SELECT count(*) as total
		 FROM pati_user
		 WHERE UserName = ? AND UserId != ?";
		$query = $this->db->query($sql, array($username, $id));
		$result = $query->result_array();
		return $result[0]['total'];
  }

  function checkPasswordByUserId($password, $id) {
		$sql = "
		 SELECT count(*) as total
		 FROM pati_user
		 WHERE UserPassword = ? AND UserId = ?";
		$result = $query->result_array();
		return $result[0]['total'];
  }

  function updatePassword($new) {
		$sql = "
		 UPDATE pati_user
		 SET UserPassword = ? 
		 WHERE UserId = ?";
		return $this->db->query($sql, array($new, $_SESSION['userid']));
  }

  function getEmptyUser() {
		$user['UserId'] = NULL;
		$user['UserUnitKerjaId'] = NULL;
		$user['UserNip'] = NULL;
		$user['UserRealName'] = NULL;
		$user['UserName'] = NULL;
		$user['UserPassword'] = NULL;
		$user['UserActive'] = 1;
		$user['UserHp'] = NULL;
		$user['UserEmail'] = NULL;
		$user['UserFoto'] = NULL;
		$user['UserExpired'] = date('d-m-Y');
		$user['UserLastLogin'] = NULL;
		$user['UserNote'] = NULL;
		return $user;
  }

}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-01-11 14:51:10 */
/* http://harviacode.com */