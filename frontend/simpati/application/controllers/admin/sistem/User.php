<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

// use App\models\eloquent\YkUser;

class User extends PATI_Controller {

    public function index() {              
        $this->load->model('User_model');
        
        $q = urldecode($this->input->get('q', TRUE));
        $start = $q ? 0 : intval($this->input->get('start'));
        
        $config['start'] = $start;
        $config['base_url'] = base_url('sistem/user');
        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['attributes'] = array('class' => 'page-link');

        $config['total_rows'] = $this->User_model->countUser($q);
        
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'rows' => $this->User_model->getUser($q, $start, $config['per_page']),
            'q' => $q,
            'search' => current_url(),
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'addes' => false,
        );

        $this->load_view('sistem/user',$data);        
    }
	
	function search() {
        $this->load->model('User_model');
        
        $aksi = '<div class="hidden-sm hidden-xs btn-group">';
//        if (has_permission('update')) {
            $aksi .= '<a href="' . base_url("sistem/user/update/$1") . '" class="btn btn-xs btn-info">'
                    . '<i class="ace-icon fa fa-pencil bigger-120"></i>'
                    . '</a>';
        
//        if (has_permission('delete')) {
            $aksi .= '<a href="javascript:;" class="btn btn-xs btn-danger" onclick="hapus($1)">'
                    . '<i class="ace-icon fa fa-trash-o bigger-120"></i>'
                    . '</a>';
        
        $aksi .='</div>';
        
        $status = $this->input->post('status');
        echo $this->User_model->getDataTables($status, $aksi);        
    }

    function add() {
        $this->load->model('User_model');
        $this->load->model('group_model');
        
        $data['user'] = $this->User_model->getEmptyUser();
        $data['groups'] = $this->group_model->getAll();
        $data['usergroup'] = array();
        $data['sub'] = 'add';
        $this->load_view('sistem/user_form', $data);
    }

    function update0($id) {
        $this->load->model('User_model');
        $this->load->model('group_model');
        $data['user'] = YkUser::with('groups')->find($id);
        
        $data['groups'] = $this->group_model->getAll();
        $data['usergroup'] = $this->User_model->getUserGroupsById($id);
        dd($data['usergroup']);
        $data['sub'] = 'update';
        $this->load_view('sistem/user_form', $data);
    }

    function update($id) {
        $this->load->model('User_model');
        $this->load->model('group_model');
        $detail = YkUser::with('groups')->find($id);
        $data['user'] = $detail;
        $data['groups'] = $this->group_model->getAll();
        $data['usergroup'] = array_map(function($item){
            return $item['GroupId'];
        }, $detail->groups->toArray());
        $data['sub'] = 'update';
        $this->load_view('sistem/user_form', $data);
    }
}

/* End of file User.php */
/* Location: ./application/controllers/sistem/User.php */
